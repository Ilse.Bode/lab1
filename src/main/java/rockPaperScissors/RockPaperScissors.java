package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run(){

        while(true) {
            //define choices
            System.out.println("Let's play round " +roundCounter);
            String human_choice = userChoice();
            String computer_choice = randomChoice();
            String choice_string = "Human chose " +human_choice+ ", computer chose " + computer_choice+ ".";

            //check who won this round
            if (isWinner(human_choice, computer_choice)) {
                System.out.println(choice_string+ " Human wins.");
                humanScore += 1;
            }
            else if (isWinner(computer_choice, human_choice)){
                System.out.println(choice_string+ " Computer wins!");
                computerScore += 1;
            }
            else {
                System.out.println(choice_string+ " It's a tie!");
            }
            System.out.println("Score: human " +humanScore+ ", computer " +computerScore);
             
            String continue_answer = continue_playing();
            if (continue_answer.equals("n")){
                break;
            }
            else if (continue_answer.equals("y")) {
                roundCounter += 1;
            }

        }
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    // check if input from user is valid
    public boolean validateInput(String input, List<String> validInput){
        input = input.toLowerCase();
        for (int i = 0; i < validInput.size(); i++){
            if (input.equals(validInput.get(i))) {
                return true;
            }
        }
        return false;
    }

    // get random choice
    public String randomChoice() { 
        Random rand = new Random();
        String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
            return randomElement; 
    }

    // get user´s choice
    public String userChoice(){
        while(true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)? ").toLowerCase();
            if (validateInput(humanChoice, rpsChoices)) {
                return humanChoice;
            }
            else {
                System.out.println("I do not understand " +humanChoice+ ". Could you try again?");
            }
        } 
    }

    // check if choice_1 wins
    public boolean isWinner(String choice_1, String choice_2){
        if (choice_1.equals("paper")){
            return choice_2.equals("rock");
        }
        else if (choice_1.equals("scissors")){
            return choice_2.equals("paper");
        }
        else {
            return choice_2.equals("scissors");
        }
    }


    // check if user wants to continue playing
    public String continue_playing() {
        while (true) {
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            List<String> validAnswers = Arrays.asList("y", "n");
            if (validateInput(continue_answer, validAnswers)) {
                return continue_answer;
            }
            else {
                System.out.println("I don't understand " + continue_answer + ". Try again.");
            }

        }
    }
    
        
}
